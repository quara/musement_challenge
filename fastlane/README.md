fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios beta
```
fastlane ios beta
```
Push a new beta build to TestFlight
### ios adhoc
```
fastlane ios adhoc
```
Create a build and sign it with adhoc provisioning and upload it on s3
### ios pull_certificates
```
fastlane ios pull_certificates
```
Pull app certificates without updating them
### ios update_certificates
```
fastlane ios update_certificates
```
Update app certificates
### ios add_devices
```
fastlane ios add_devices
```
Add devices to provisioning profiles

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
