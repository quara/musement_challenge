//
//  ActivityDetailViewController.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 03/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import UIKit
import Kingfisher

class ActivityDetailViewController: UIViewController {
    
    var activity: Activity! {
        didSet {
            guard let _ = self.view else {
                return
            }
            
            updateView()
        }
    }

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var aboutLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "activity_title".localized
        
        fetchActivity()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateView()
    }
    
    func updateView() {
        imageView.setImage(with: activity.coverImageUrl, placeholder: #imageLiteral(resourceName: "Placeholder"))
        titleLabel.text = activity.title
        priceLabel.text = activity.retailPrice.formattedValue
        aboutLabel.text = activity.about
    }

}

// network
extension ActivityDetailViewController {
    
    func fetchActivity() {
        Activity.fetchActivity(uuid: activity.uuid) { result in
            switch result {
            case .success(let activity):
                self.activity = activity
            case .failure(let error):
                print(error)
            }
        }
    }
    
}
