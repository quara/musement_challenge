//
//  ActivitiesViewController.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 03/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import UIKit

class ActivitiesViewController: UIViewController {
    
    static let searchMinCharCount = 3
    static let searchDelay = 1.0
    
    @IBOutlet weak var placeholderView: UIView!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var emptyListView: UIView!
    @IBOutlet weak var emptyListLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var searchBar: UISearchBar!
    
    var activityList: ListResult<Activity>? {
        didSet {
            updateView()
        }
    }
    var searchText: String? {
        didSet {
            activityList = nil
            
            // cancel previous search task
            searchTask?.cancel()
            searchTask = nil
            
            guard let searchText = searchText,
                searchText.count >= ActivitiesViewController.searchMinCharCount else {
                return
            }
            
            loading = true
            
            // execute search task after delay
            searchTask = DispatchWorkItem {
                self.fetchActivities(searchText: searchText)
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + ActivitiesViewController.searchDelay, execute: searchTask!)
        }
    }
    var searchTask: DispatchWorkItem?
    var loading = false {
        didSet {
            updateView()
        }
    }
    var selectedActivity: Activity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar = UISearchBar()
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        navigationItem.titleView = searchBar

        self.title = "actitivies_title".localized
        
        self.placeholderLabel.text = "actitivies_placeholder_label".localized
        self.emptyListLabel.text = "actitivies_empty_list_label".localized
        
        // register tableview cells
        tableView.register(ActivityCell.self)
        
        updateView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showActivityDetail":
            guard let selectedActivity = selectedActivity else {
                assertionFailure("no selected activity")
                return
            }
            guard let activityDetailViewController = segue.destination as? ActivityDetailViewController else {
                assertionFailure("view controller is not ActivityDetailViewController")
                return
            }
            
            activityDetailViewController.activity = selectedActivity
            self.selectedActivity = nil
        default:
            break
        }
    }
    
    func updateView() {
        if loading {
            loadingView.isHidden = false
            
            placeholderView.isHidden = true
            emptyListView.isHidden = true
            tableView.isHidden = true
            return
        }
        guard let activityList = activityList else {
            placeholderView.isHidden = false
            
            loadingView.isHidden = true
            emptyListView.isHidden = true
            tableView.isHidden = true
            
            tableView.reloadData()
            
            return
        }
        if activityList.data.isEmpty {
            emptyListView.isHidden = false
            
            placeholderView.isHidden = true
            loadingView.isHidden = true
            tableView.isHidden = true
            return
        }
        
        
        tableView.isHidden = false
        
        placeholderView.isHidden = true
        emptyListView.isHidden = true
        
        tableView.reloadData()
    }
    
    @IBAction func viewTap(sender: Any) {
        hideSearchBarKeyboard()
    }
    
    func hideSearchBarKeyboard() {
        searchBar.resignFirstResponder()
    }
    
    func showActivityDetail(activity: Activity) {
        self.selectedActivity = activity
        performSegue(withIdentifier: "showActivityDetail", sender: self)
    }
    
}

extension ActivitiesViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchText = searchText
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        hideSearchBarKeyboard()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        hideSearchBarKeyboard()
    }
    
}

// network
extension ActivitiesViewController {
    
    func fetchActivities(searchText: String) {
        loading = true
        
        Activity.fetchActivities(searchText: searchText) { result in
            self.loading = false
            
            switch result {
            case .success(let activityList):
                guard let lastSearchedText = self.searchText,
                    lastSearchedText == searchText else {
                        // search text changed discard results
                        return
                }
                
                self.activityList = activityList
            case .failure(let error):
                print(error)
            }
        }
    }
    
}

// tableview
extension ActivitiesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let activities = activityList?.data else {
            return 0
        }
        
        return activities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ActivityCell.nameIdentifier, for: indexPath)
        guard let activityCell = cell as? ActivityCell else {
            assertionFailure("cell is not ActivityCell")
            return cell
        }
        
        guard let activities = activityList?.data else {
            assertionFailure("activities is nil")
            return activityCell
        }
        
        let activity = activities[indexPath.row]
        
        activityCell.activity = activity
        
        return cell
    }
    
}

extension ActivitiesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        hideSearchBarKeyboard()

        guard let activities = activityList?.data else {
            assertionFailure("activities is nil")
            return
        }
        
        let activity = activities[indexPath.row]
        
        showActivityDetail(activity: activity)
    }
    
}

extension ActivitiesViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        hideSearchBarKeyboard()
    }
    
}
