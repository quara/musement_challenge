//
//  Router.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 03/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    
    case searchActivities(text: String)
    case activity(activityUuid: String)
    
    
    func asURLRequest() throws -> URLRequest {
        let url = try (apiBaseUrl + path).asURL()
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = httpHeaders
        
        // query parameters and body encoding
        switch self {
        case .searchActivities(let text):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: ["text" : text])
        case .activity:
            break
        }
        
        return urlRequest
    }

    
    var method: HTTPMethod {
        switch self {
        case .searchActivities,
             .activity:
            return .get
        }
    }
    
    var httpHeaders: [String : String] {
        let headers = [String : String]()
        switch self {
        default:
            break
        }
        
        return headers
    }
    
    var path: String {
        switch self {
        case .searchActivities:
            return "/activities"
        case .activity(let activityUuid):
            return "/activities/\(activityUuid)"
        }
    }
    
}
