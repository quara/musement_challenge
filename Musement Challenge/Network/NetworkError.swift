//
//  NetworkError.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 03/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case failedMapping(responseData: Any)
}
