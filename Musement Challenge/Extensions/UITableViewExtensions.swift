//
//  UITableViewExtensions.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 03/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    public func register<T: UITableViewCell>(_: T.Type){
        let nameIdentifier = T.nameIdentifier
        let nib = UINib(nibName: nameIdentifier, bundle: Bundle.main)
        self.register(nib, forCellReuseIdentifier: nameIdentifier)
    }
    
}
