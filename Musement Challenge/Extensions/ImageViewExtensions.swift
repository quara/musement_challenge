//
//  ImageViewExtensions.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 04/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension ImageView {
    
    func setImage(with url: String?, placeholder: UIImage?) {
        if let urlString = url,
            let url = URL(string: urlString) {
            kf.setImage(with: url, placeholder: placeholder)
        }
        else if let placeholder = placeholder {
            image = placeholder
        }
        else {
            image = nil
        }
    }
    
}
