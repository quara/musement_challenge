//
//  UITableViewCellExtensions.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 03/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
    
    // return name identifier
    public class var nameIdentifier: String {
        return String(describing: self)
    }
    
}
