//
//  File.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 04/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import UIKit

class Theme {
    
    static func apply() {
        UILabel.appearance().font = regularLabelFont
        UITextField.appearance().font = regularLabelFont
        
        RegularLabel.appearance().textColor = regularLabelColor
        RegularLabel.appearance().font = regularLabelFont
        
        TitleLabel.appearance().font = titleLabelFont
        
        InfoLabel.appearance().font = infoLabelFont
        
        PriceLabel.appearance().textColor = priceLabelColor
    }
    
}
