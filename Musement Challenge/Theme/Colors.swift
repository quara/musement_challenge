//
//  Colors.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 04/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import UIKit

let regularLabelColor =                   UIColor(red:0.26, green:0.26, blue:0.26, alpha:1.0)
let infoLabelColor =                      UIColor(red:0.57, green:0.57, blue:0.57, alpha:1.0)
let priceLabelColor =                     UIColor(red:1.00, green:0.49, blue:0.47, alpha:1.0)
