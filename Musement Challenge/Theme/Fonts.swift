//
//  Fonts.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 04/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import UIKit

let regularLabelFont =                      UIFont(name: "AvenirNext-Medium", size: 14)
let titleLabelFont =                        UIFont(name: "AvenirNext-Medium", size: 18)
let infoLabelFont =                         UIFont(name: "AvenirNext-Medium", size: 12)
