//
//  UILableSubclasses.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 04/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import UIKit

class RegularLabel: UILabel {}
class TitleLabel: RegularLabel {}
class InfoLabel: RegularLabel {}
class PriceLabel: RegularLabel {}
