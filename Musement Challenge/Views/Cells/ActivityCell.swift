//
//  ActivityCell.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 03/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import UIKit

class ActivityCell: UITableViewCell {

    @IBOutlet weak var activityImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var activity: Activity? {
        didSet {            
            updateView()
        }
    }
    
    func updateView() {
        guard let activity = activity else {
            return
        }
        
        activityImageView.setImage(with: activity.coverImageUrl, placeholder: #imageLiteral(resourceName: "Placeholder"))
        titleLabel.text = activity.title
        priceLabel.text = activity.retailPrice.formattedValue
    }
    
}
