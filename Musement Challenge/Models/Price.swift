//
//  Price.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 03/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import ObjectMapper

class Price: Mappable {
    
    var value: Int!
    var currency: String!
    var formattedValue: String!
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        value               <- map["value"]
        currency            <- map["currency"]
        formattedValue      <- map["formatted_value"]
  }
    
}
