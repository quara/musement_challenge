//
//  Meta.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 03/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import ObjectMapper

class Meta: Mappable {
    
    var count: Int!
    var matchType: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        count               <- map["count"]
        matchType           <- map["match_type"]
    }

}
