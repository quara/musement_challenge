//
//  Activity.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 03/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

class Activity: Mappable {
    
    var uuid: String!
    var title: String!
    var description: String!
    var about: String!
    var retailPrice: Price!
    var coverImageUrl: String?
        
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        uuid                <- map["uuid"]
        title               <- map["title"]
        description         <- map["title"]
        about               <- map["about"]
        retailPrice         <- map["retail_price"]
        coverImageUrl       <- map["cover_image_url"]
    }
}

// MARK: Network

extension Activity {
    
    static func fetchActivity(uuid: String, completionHandler: @escaping (Result<Activity>) -> Void) {
        Alamofire.request(Router.activity(activityUuid: uuid)).responseJSON { (response) in
            switch response.result {
            case .success(let data):
                guard let json = data as? [String : Any],
                    let activity = Activity(JSON: json) else {
                        let failedMappingError = NetworkError.failedMapping(responseData: data)
                        completionHandler(.failure(failedMappingError))
                        
                        return
                }
                
                completionHandler(.success(activity))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
    
    static func fetchActivities(searchText: String, completionHandler: @escaping (Result<ListResult<Activity>>) -> Void) {
        Alamofire.request(Router.searchActivities(text: searchText)).responseJSON { (response) in
            switch response.result {
            case .success(let data):
                guard let json = data as? [String : Any],
                    let activityListResult = ListResult<Activity>(JSON: json) else {
                        let failedMappingError = NetworkError.failedMapping(responseData: data)
                        completionHandler(.failure(failedMappingError))
                        
                        return
                }
                
                completionHandler(.success(activityListResult))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
    
}
