//
//  ListResult.swift
//  Musement Challenge
//
//  Created by Enrico Quarantini on 03/05/2018.
//  Copyright © 2018 Enrico Quarantini. All rights reserved.
//

import Foundation
import ObjectMapper

class ListResult<Element>: Mappable where Element: Mappable {
    
    var meta: Meta!
    var data: [Element]!
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        meta                <- map["meta"]
        data                <- map["data"]
    }
    
}
